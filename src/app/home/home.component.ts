import {Component, OnInit} from '@angular/core';
import {Facade} from "../+state/facade";
import {map, Observable, of} from "rxjs";
import {IApplication, IPaginationModel} from "../+state/model";
import * as moment from "moment/moment";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [Facade]
})
export class HomeComponent implements OnInit {

  tableColumns: Column[] = [
    {label: 'Name(email)', key: 'nameAndEmail', sorted: false},
    {label: 'Phone', key: 'phone', sorted: false},
    {label: 'Job', key: '_job', sorted: false},
    {label: 'Status', key: 'status', sorted: false},
    {label: '', key: 'action', sorted: false},
  ];

  totalItems$ = this.facade.data$

  activeItems$ = this.facade.activeData$.pipe(
    map((data: IApplication[]) => {
      if (!data.length) {
        this.facade.loadData()
        return
      }
      return data.map(item => {
        return {
          nameAndEmail: `${item.name}(${item.email})`,
          phone: item.phone,
          _job: item._job.title,
          status: item.status,
          createdAt: item.created_at,
          resume: item.resume
        }
      })
    })
  )

  pagination$: Observable<IPaginationModel<IApplication>> = this.facade.pagination$

  activePage$ = this.facade.activePage$

  constructor(private facade: Facade) {
  }

  ngOnInit(): void {
  }

  getData(row: any, key: string): string {
    return row[key]
  }

  handleDate(date: string): string {
    return moment(new Date(date)).fromNow()
  }

  onPageClick(page: number, pagination: IPaginationModel<IApplication>, totalItems: number) {
    if ((page * 4) > totalItems) {
      this.facade.loadMoreData(page)
    } else {
      this.facade.changePage(page)
    }
  }

  onSortClick(column: Column, items: any[]) {
    let tempRows: IApplication[] = []
    items.map(item => {
      const keys = Object.keys(item)
      keys.map(key => {
        if (key === column.key) {
          tempRows =
            items.sort((n1: any, n2: any) => {
              if (n1[key] > n2[key]) {
                return column.sorted ? -1 : 1;
              }

              if (n1[key] < n2[key]) {
                return column.sorted ? 1 : -1;
              }

              return 0;
            })
        }
      })
    })

    column.sorted = !column.sorted

    this.tableColumns.map(item => {
      if (item.key !== column.key) {
        item.sorted = false
      }
    })

    this.activeItems$ = of(tempRows) as any
  }
}

export interface Column {
  label: string;
  key: string;
  sorted: boolean;
}
