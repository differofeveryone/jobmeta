import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NetworkService} from "../network.service";
import {Subscription, tap} from "rxjs";
import {HttpErrorResponse} from "@angular/common/http";
import {ToastrService} from "ngx-toastr";
import {IApplicantRequest, IFileUploadResponse} from "../model";
import {Router} from "@angular/router";

@Component({
  selector: 'app-add-application',
  templateUrl: './add-application.component.html',
  styleUrls: ['./add-application.component.scss']
})
export class AddApplicationComponent implements OnInit, OnDestroy {

  form: FormGroup

  jobList$ = this.networkService.getJobs()

  statusList = [{title: 'pending_review'}]

  chooseFileTitle = 'Choose File'

  fileUploadResponse: IFileUploadResponse | undefined

  postApplicationSubscription = new Subscription()

  constructor(
    private formBuilder: FormBuilder,
    private networkService: NetworkService,
    private router: Router,
    private toastService: ToastrService
  ) {
    this.form = formBuilder.group({
      fullName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: '',
      status: 'pending_review',
      job: 1
    })
  }

  ngOnInit(): void {
  }

  ngOnDestroy() {
    this.postApplicationSubscription.unsubscribe()
  }

  onFileSelected(event: any) {
    if (event.target.files && event.target.files.length) {
      const file: File = event.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      this.chooseFileTitle = file.name

      const formData = new FormData()
      formData.set('content', file, file.name)
      this.networkService.uploadFile(formData).pipe(
        tap((res) => {
          this.fileUploadResponse = res
          this.toastService.success('File uploaded successfully')
        }, (error: HttpErrorResponse) => {
          this.toastService.error(error.error.message)
        })
      ).subscribe()

      reader.onload = () => {
        const img = new Image()
        img.src = reader.result as string
        let height: number
        let width: number
        img.onload = async () => {
          height = img.naturalHeight
          width = img.naturalWidth
        }
      }
    }
  }

  submit() {
    if (this.form.invalid) {
      return
    }

    const payload: IApplicantRequest = {
      job_id: this.form.value.job,
      email: this.form.value.email,
      name: this.form.value.fullName,
      phone: this.form.value.phone,
      status: this.form.value.status, resume: this.fileUploadResponse
    }

    this.postApplicationSubscription =
      this.networkService.postApplication(payload)
        .pipe(
          tap(() => {
            this.toastService.success('New Application submitted successfully')
            this.router.navigate(['']).then()
          }, (error: HttpErrorResponse) => {
            console.log(error)
            this.toastService.error(error.error.message)
          })
        ).subscribe()
  }
}
