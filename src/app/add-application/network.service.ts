import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "@environments/environment";
import {map, Observable} from "rxjs";
import {IApplicantRequest, ICategory, IFileUploadResponse, IJobRequest} from "./model";
import {IJob, IPaginationModel} from "../+state/model";

@Injectable({
  providedIn: 'root'
})
export class NetworkService {

  constructor(private httpClient: HttpClient) {
  }

  getCategories(): Observable<ICategory[]> {
    return this.httpClient.get<ICategory[]>(
      `${environment.baseUrl}category`
    )
  }

  getJobs(): Observable<IJob[]> {
    return  this.httpClient.get<IPaginationModel<IJob>>(
      `${environment.baseUrl}job`
    ).pipe(
      map(response => response.items)
    )
  }

  addJob(data: IJobRequest): Observable<any>{
    return this.httpClient.post(
      `${environment.baseUrl}job`,
      data
    )
  }

  postApplication(data: IApplicantRequest): Observable<any> {
    return this.httpClient.post(
      `${environment.baseUrl}application`,
      data
    )
  }

  uploadFile(formData: FormData): Observable<IFileUploadResponse> {
    return this.httpClient.post<IFileUploadResponse>(
      `${environment.baseUrl}upload/attachment`,
      formData
    )
  }
}
