import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NetworkService} from "../network.service";
import {IJobRequest} from "../model";
import {Subscription, tap} from "rxjs";
import {Router} from "@angular/router";
import {HttpErrorResponse} from "@angular/common/http";
import {ToastrService} from 'ngx-toastr'

@Component({
  selector: 'app-add-job',
  templateUrl: './add-job.component.html',
  styleUrls: [
    './add-job.component.scss',
    './../add-application/add-application.component.scss'
  ]
})
export class AddJobComponent implements OnInit, OnDestroy, AfterViewInit {

  form: FormGroup

  categories$ = this.networkService.getCategories()

  addJobSubscription = new Subscription()

  constructor(
    private formBuilder: FormBuilder,
    private networkService: NetworkService,
    private router: Router,
    private toastService: ToastrService
  ) {
    this.form = formBuilder.group({
      title: ['', Validators.required],
      type: ['', Validators.required],
      location: {
        type: "point",
        data: {
          lng: -118.2436849,
          lat: 34.0522342
        }
      },
      category: 1,
      description: '',
    })
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
  }

  ngOnDestroy() {
    this.addJobSubscription.unsubscribe()
  }

  submit() {
    if (this.form.invalid) {
      return
    }

    const payload: IJobRequest = {
      title: this.form.value.title,
      type: this.form.value.type,
      location: this.form.value.location,
      description: this.form.value.description,
      category: {category_id: this.form.value.category, category: ''}
    }

    this.addJobSubscription = this.networkService.addJob(payload).pipe(
      tap(() => {
        this.router.navigate(['add-application']).then()
        this.toastService.success('Job added successfully')
      }, (error: HttpErrorResponse) => {
        this.toastService.error(error.error.message)
      })
    ).subscribe()
  }
}
