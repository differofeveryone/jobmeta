import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AddApplicationRoutingModule} from './add-application-routing.module';
import {AddApplicationComponent} from './add-application/add-application.component';
import {AddJobComponent} from './add-job/add-job.component';
import {ReactiveFormsModule} from "@angular/forms";
import {ToastrModule, ToastrService} from "ngx-toastr";


@NgModule({
  declarations: [
    AddApplicationComponent,
    AddJobComponent
  ],
  imports: [
    CommonModule,
    AddApplicationRoutingModule,
    ReactiveFormsModule,
    ToastrModule.forRoot()
  ],
  providers: [ToastrService]
})
export class AddApplicationModule {
}
