import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AddApplicationComponent} from "./add-application/add-application.component";
import {AddJobComponent} from "./add-job/add-job.component";

const routes: Routes = [
  {path: '', component: AddApplicationComponent},
  {path: 'add-job', component: AddJobComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddApplicationRoutingModule { }
