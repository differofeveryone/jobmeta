import {IJobLocation} from "../+state/model";

export interface ICategory {
  id: number
  created_at: number
  category: string
}

export interface IJobRequest {
  title: string
  description: string
  type: string
  location: IJobLocation
  category: IJobCategory
}

export interface IJobCategory {
  category: string
  category_id: number
}

export interface IApplicantRequest {
  job_id: number
  name: string
  phone: string
  email: string
  status: string
  resume?: {
    path: string
    name: string
    type: string
    size: number
    mime: string
    meta: {}
  }
}

export interface IFileUploadResponse {
  meta: {
    width: number,
    height: number
  }
  mime: string
  name: string
  path: string
  size: number
  type: string
}
