import {createReducer, on} from "@ngrx/store";
import {changePage, dataLoadedSuccessfully, loadData, loadMoreData, moreDataLoadedSuccessfully} from "./action";
import {adapter, initialState} from "./entity";
import {Selector} from "./selector";
import {IApplication, IPaginationModel} from "./model";

export const reducer = createReducer(
  initialState,
  on(loadData, (state) => {
    return {
      ...state,
      dataLoaded: false,
      success: false
    }
  }),
  on(dataLoadedSuccessfully, (state, action) => {
      const {items} = action.data
      return adapter.setAll(items, {
        ...state,
        dataLoaded: true,
        success: true,
        pagination: action.data,
        activeData: items,
        loadedItems: items.length,
        activePage: action.data.curPage
      })
    }
  ),
  on(loadMoreData, (state) => {
    return {
      ...state,
      dataLoaded: false,
      success: false
    }
  }),
  on(moreDataLoadedSuccessfully, (state, action) => {
      const {items} = action.data
      return adapter.addMany(items, {
        ...state,
        success: true,
        pagination: action.data,
        activeData: items,
        loadedItems: state.loadedItemsCount + items.length,
        activePage: action.data.curPage
      })
    }
  ),
  on(changePage, (state, action) => {
      const page = action.page
      const allItems = [...Selector.selectAll(state)];
      let activeData: IApplication[] = []
      for (let i = (page - 1) * 4; i < page * 4; i++) {
        activeData.push(allItems[i])
      }
      let pagination: IPaginationModel<IApplication> = <IPaginationModel<IApplication>>{}
      if (action.page === 1) {
        pagination = {
          ...state.pagination,
          curPage: action.page,
          prevPage: null,
          nextPage: 2
        }
      } else if (action.page === state.pagination.pageTotal) {
        pagination = {
          ...state.pagination,
          curPage: action.page,
          nextPage: null,
          prevPage: 1
        }
      }
      return {
        ...state,
        success: true,
        activeData,
        activePage: action.page,
        pagination
      }
    }
  )
)
