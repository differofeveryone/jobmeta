export interface IPaginationModel<T> {
  itemsReceived: number
  curPage: number
  nextPage: number | null
  prevPage: number | null
  itemsTotal: number
  pageTotal: number
  items: T[]
}

export interface IApplication {
  id: number
  created_at: number
  job_id: number
  name: string
  phone: number
  email: string
  status: string
  resume: IResume
  _job: IJob
}

export interface IResume {
  path: string
  name: string
  type: string
  size: number
  mime: string
  meta: IResumeMetaData
  url: string
}

export interface IResumeMetaData {
  width: number
  height: number
}

export interface IJob {
  id: number
  created_at: number
  title: string
  type: string
  location: IJobLocation
}

export interface IJobLocation {
  type: string
  data: IJobLocationData
}

export interface IJobLocationData {
  lng: number
  lat: number
}
