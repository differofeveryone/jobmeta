import {Injectable} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {ApplicationState} from "./entity";
import {Selector} from "./selector";
import {changePage, loadData, loadMoreData} from "./action";

@Injectable()
export class Facade {

  data$ = this.store.pipe(select(Selector.selectData))
  activeData$ = this.store.pipe(select(Selector.selectActiveData))
  activePage$ = this.store.pipe(select(Selector.selectActivePage))
  pagination$ = this.store.pipe(select(Selector.selectPagination))
  loadedItemsCount$ = this.store.pipe(select(Selector.selectLoadedItemsCount))

  constructor(private store: Store<ApplicationState>) {
  }

  loadData() {
    this.store.dispatch(loadData())
  }

  loadMoreData(page: number) {
    this.store.dispatch(loadMoreData({page}))
  }

  changePage(page: number) {
    this.store.dispatch(changePage({page}))
  }
}
