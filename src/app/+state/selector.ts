import {adapter, ApplicationState} from "./entity";
import {createFeatureSelector, createSelector} from "@ngrx/store";

export class Selector {

  static selector = createFeatureSelector<ApplicationState>('reducer')

  static selectAll = adapter.getSelectors().selectAll

  static selectData = createSelector(
    Selector.selector,
    Selector.selectAll
  )

  static selectPagination = createSelector(
    Selector.selector,
    (state) => state.pagination
  )

  static selectLoadedItemsCount = createSelector(
    Selector.selector,
    (state) => state.loadedItemsCount
  )

  static selectActivePage = createSelector(
    Selector.selector,
    (state) => state.activePage
  )

  static selectActiveData = createSelector(
    Selector.selector,
    (state) => state.activeData
  )
}
