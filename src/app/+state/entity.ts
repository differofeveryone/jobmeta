import {IApplication, IPaginationModel} from "./model";
import {createEntityAdapter, EntityState} from "@ngrx/entity";

export const APPLICATION_FEATURE_KEY = 'application';

export interface ApplicationState extends EntityState<IApplication> {
  loadedItemsCount: number
  dataLoaded: boolean
  success: boolean
  pagination: IPaginationModel<IApplication>
  activeData: IApplication[]
  activePage: number
}

export const adapter = createEntityAdapter<IApplication>()

export const initialState = adapter.getInitialState<ApplicationState>({
  activePage: 0,
  loadedItemsCount: 0,
  dataLoaded: false,
  success: false,
  pagination: {},
  activeData: new Array<IApplication>()
} as ApplicationState)
