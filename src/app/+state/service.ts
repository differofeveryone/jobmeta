import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {IApplication, IPaginationModel} from "./model";
import {environment} from "@environments/environment";

@Injectable()
export class Service {

  constructor(private httpClient: HttpClient) {
  }

  getApplications(page = 1): Observable<IPaginationModel<IApplication>> {
    return this.httpClient.get<IPaginationModel<IApplication>>(
      `${environment.baseUrl}application?external[page]=${page}`
    )
  }
}
