import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Service} from "./service";
import {dataLoadedSuccessfully, loadData, loadMoreData, moreDataLoadedSuccessfully} from "./action";
import {catchError, map, mergeMap, of} from "rxjs";

@Injectable()
export class Effect {

  loadData$ = createEffect(() => this.actions$.pipe(
    ofType(loadData),
    mergeMap(() => this.networkService.getApplications().pipe(
      map(response =>
        dataLoadedSuccessfully({data: response})
      ),
      catchError(err => of(err))))
  ))

  loadMoreData$ = createEffect(() => this.actions$.pipe(
    ofType(loadMoreData),
    mergeMap(({page}) => this.networkService.getApplications(page).pipe(
      map(response =>
        moreDataLoadedSuccessfully({data: response})
      ),
      catchError(err => of(err))))
  ))

  constructor(private actions$: Actions, private networkService: Service) {
  }
}
