import {createAction, props} from "@ngrx/store";
import {IApplication, IPaginationModel} from "./model";

export const loadData = createAction('[App] load data')

export const dataLoadedSuccessfully = createAction(
  '[App] data loaded successfully',
  props<{ data: IPaginationModel<IApplication> }>()
)

export const loadMoreData = createAction('[App] load more data',
  props<{page: number}>()
)

export const moreDataLoadedSuccessfully = createAction(
  '[App] more data loaded successfully',
  props<{ data: IPaginationModel<IApplication> }>()
)

export const changePage = createAction('[App] change table page',
  props<{page: number}>()
)
