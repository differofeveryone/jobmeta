import {Component, OnInit} from '@angular/core';
import {environment} from "@environments/environment";
import {ActivationEnd, Router} from "@angular/router";
import {tap} from "rxjs";

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  isToolbarOptionsEnable = false

  brandName = environment.appBrandName

  constructor(private router: Router) {
    router.events.pipe(
      tap((event) => {
        if (event instanceof ActivationEnd) {
          this.isToolbarOptionsEnable =
            event.snapshot.routeConfig?.path === 'home'
        }
      })
    ).subscribe()
  }

  ngOnInit(): void {
  }
}
